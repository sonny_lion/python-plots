#!/usr/bin/python
# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# Filename:    shared_x_axis.py
# -----------------------------------------------------------------------------  
#
# Sonny LION - sonny.lion@obspm.fr
# Doctorant - PhD Student
# Laboratoire d'Etudes Spatiales et d'Instrumentation en Astrophysique (LESIA)
# -----------------------------------------------------------------------------


import matplotlib.pyplot as plt
import numpy


def waveform(time,(x,y,z),(label_x,label_y,label_z),title=""):

	# Three subplots sharing both x/y axes

	f, (ax1, ax2, ax3) = plt.subplots(3, sharex=True, sharey=True)
	ax1.plot(time, x)
	ax1.set_title(title)
	ax2.plot(time, y)
	ax3.plot(time, z)
	ax1.set_ylabel(label_x)
	ax2.set_ylabel(label_y)
	ax3.set_ylabel(label_z)

	# Fine-tune figure; make subplots close to each other and hide x ticks for
	# all but bottom plot.

	f.subplots_adjust(hspace=0)
	plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)
	return (ax1, ax2, ax3)


# Simple data to display in various forms
x = numpy.linspace(0, 2 * numpy.pi, 400)
y = numpy.sin(x ** 2)

waveform(x,(y,y+2.0*x,y/2.0+x),(r"B$_x$",r"B$_y$",r"B$_z$"))

plt.show()