#!/usr/bin/python
# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# Filename:    histo.py
# -----------------------------------------------------------------------------  
#
# Sonny LION - sonny.lion@obspm.fr
# Doctorant - PhD Student
# Laboratoire d'Etudes Spatiales et d'Instrumentation en Astrophysique (LESIA)
# -----------------------------------------------------------------------------

import matplotlib.pyplot as plt
import matplotlib
import numpy

# Data test

x=numpy.linspace(-5,5,100)

# Simple plot

fig = plt.figure(figsize=(12, 8), dpi=100) # figure layout, resizable

bins=numpy.logspace(-5,5,20)

matplotlib.rcParams.update({'font.size': 16}) # global font size
p1=plt.hist(numpy.sin(x),bins=bins,label="sin(x)",color="blue")
plt.legend() # show the legend
plt.title("Fonctions trigonometriques")
plt.xlabel('Donnees')
plt.ylabel('Probabilite')
plt.xscale('log')
ax = plt.axes()
plt.text(0.1, 0.3,"Test",\
 verticalalignment='center',transform=ax.transAxes) # Some text, coordinate in the plot reference frame
plt.show()